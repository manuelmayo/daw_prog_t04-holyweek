//T04_HolyWeek_02

public class Sort {
	
	public static int[] bubbleSort(int[] array) {
		Boolean cambio = true;
		while (cambio) {
			cambio = false;
			for (int i=0;i<array.length-1;i++) {
				if (array[i] > array[i+1]) {
					int temp = array[i];
					array[i] = array[i+1];
					array[i+1] = temp;
					cambio = true;
				}
			}
		}
		return array;
	}
	
}
