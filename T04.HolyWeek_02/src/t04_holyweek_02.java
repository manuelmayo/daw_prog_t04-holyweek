//T04_HolyWeek_02

import java.util.Scanner;
import java.util.Random;

public class t04_holyweek_02 {
	
	private static Scanner ENTRADA = new Scanner (System.in);
	private static Random RANDOM = new Random();

	public static void main(String[] args) {
		
		System.out.print("De cuantos números quieres la lista: ");
		
		int tamLista = ENTRADA.nextInt();
		
		int[] arrayNumeros = new int[tamLista];
		
		System.out.print("Añadiendo... ");
		for (int i=0;i<tamLista;i++) {
			int randomNumber =  RANDOM.nextInt(100);
			System.out.print(randomNumber+" ");
			arrayNumeros[i] = randomNumber;
		}
		System.out.println();
		
		int[] arrayOrdenado = Sort.bubbleSort(arrayNumeros);
		
		System.out.print("La lista ordenada sería: ");
		for (int i=0;i<tamLista;i++) {
			System.out.print(arrayOrdenado[i]+" ");
		}
	}
}
