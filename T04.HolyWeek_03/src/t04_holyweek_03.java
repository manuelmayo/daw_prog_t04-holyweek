//T04_HolyWeek_03

import java.util.Scanner;
import java.util.Random;

public class t04_holyweek_03 {
	
	private static Scanner ENTRADA = new Scanner (System.in);
	private static Random RANDOM = new Random();
	private static String ABCD = "ABCDEFGHIJKLMÑNOPQRST";
	
	public static void main(String[] args) {
		
		System.out.print("1) Pila\n2) Cola\n> ");
		int opc = ENTRADA.nextInt();
		
		if (opc == 1 || opc == 2) {
			String tipo = new String[] {"","pila","cola"}[opc];
			System.out.printf("De cuantos caracteres quieres la %s: ",tipo);
			int tamLista = ENTRADA.nextInt();
			
			if (opc == 1) { //PILA
				Pila estructura = new Pila(tamLista);
				for (int i=0;i<tamLista+2;i++) {
					char randomCar =  ABCD.charAt(RANDOM.nextInt(ABCD.length()));
					System.out.printf("Añadiendo ...%s\n",randomCar);
					estructura.push(randomCar);
				}
				
				for (int i=0;i<tamLista+2;i++) {
					char charPop = estructura.pop();
					if (charPop != 0) {
						System.out.printf("Quitando ...%s\n",charPop);
					}
				}
			} else { //COLA
				Cola estructura = new Cola(tamLista);
				for (int i=0;i<tamLista+2;i++) {
					char randomCar =  ABCD.charAt(RANDOM.nextInt(ABCD.length()));
					System.out.printf("Añadiendo ...%s\n",randomCar);
					estructura.push(randomCar);
				}
				
				for (int i=0;i<tamLista+2;i++) {
					char charPop = estructura.pop();
					if (charPop != 0) {
						System.out.printf("Quitando ...%s\n",charPop);
					}
				}
			}
			
		} else {
			System.out.println("Opción inválida");
		}
	}
}
