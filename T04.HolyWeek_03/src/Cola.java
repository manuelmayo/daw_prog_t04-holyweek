//T04_HolyWeek_03

import java.lang.IllegalArgumentException;

public class Cola {
	
	char[] estructura;
	int selec = 0;
	int tam = 0;
	
	public Cola(int tam) {
		if (tam>0) {
			this.estructura = new char[tam];
			this.tam = tam;
		} else {
			throw new IllegalArgumentException("El argumento debe ser mayor que 0");
		}
	}
	
	public void push(char c) {
		if (selec < this.tam) {
			this.estructura[selec] = c;
			this.selec++;
		} else {
			System.out.println("No queda espacio en la cola");
		}
	}
	
	public char pop() {
		if (this.selec > 0) {
			char[] newEstructura = new char[this.tam];
			for (int i=0;i<this.selec-1;i++) {
				newEstructura[i] = this.estructura[i+1];
			}
			char elementPop = this.estructura[0];
			this.estructura = newEstructura;
			this.selec--;
			return elementPop;
		} else {
			System.out.println("La cola está vacia");
			return 0;
		}
	}
	
}
