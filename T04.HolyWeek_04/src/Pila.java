//T04_HolyWeek_03

import java.lang.IllegalArgumentException;

public class Pila {
	
	char[] estructura;
	int selec = 0;
	int tam = 0;
	
	public Pila(int tam) {
		if (tam>0) {
			this.estructura = new char[tam];
			this.tam = tam;
		} else {
			throw new IllegalArgumentException("El argumento debe ser mayor que 0");
		}
	}
	
	public void push(char c) {
		if (selec < this.tam) {
			this.estructura[selec] = c;
			this.selec++;
		} else {
			this.tam++;
			this.selec++;
			char[] newEstructura = new char[this.tam];
			for (int i=0;i<this.tam-1;i++) {
				newEstructura[i] = this.estructura[i];
			}
			newEstructura[this.tam-1] = c;
			this.estructura = newEstructura;
		}
	}
	
	public char pop() {
		if (this.selec > 0) {
			char[] newEstructura = new char[this.tam];
			for (int i=0;i<this.selec-1;i++) {
				newEstructura[i] = this.estructura[i];
			}
			char elementPop = this.estructura[this.selec-1];
			this.estructura = newEstructura;
			this.selec--;
			return elementPop;
		} else {
			System.out.println("La cola está vacia");
			return 0;
		}
	}
	
}
