//T04_HolyWeek_01

public class t04_holyweek_01 {

	public static void main(String[] args) {
		
		Complejo c1 = new Complejo(2,8);
		Complejo c2 = new Complejo(10,10);
		
		System.out.printf("c1 [Real]: %s\n",c1.consulta_Real());
		System.out.printf("c1 [Imag]: %s\n",c1.consulta_Imag());
		
		c1.cambia_Real(20);
		c1.cambia_Imag(5);
		
		System.out.printf("c1 [newReal]: %s\n",c1.consulta_Real());
		System.out.printf("c1 [newImag]: %s\n",c1.consulta_Imag());
		
		System.out.println("c1: "+c1.toString());
		System.out.println("c2: "+c2.toString());
		
		c1.sumar(c2);
		System.out.printf("c1 + c2: "+c1.toString());
	}
}
