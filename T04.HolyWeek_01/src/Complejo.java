//T04_HolyWeek_01

public class Complejo {
	public double real;
	public double imag;
	
	public Complejo() {
		this.real = 0;
		this.imag = 0;
	}
	
	public Complejo(double real,double imag) {
		this.real = real;
		this.imag = imag;
	}
	
	public double consulta_Real() {
		return this.real;
	}
	
	public double consulta_Imag() {
		return this.imag;
	}
	
	public void cambia_Real(double newReal) {
		this.real = newReal;
	}
	
	public void cambia_Imag(double newImag) {
		this.imag = newImag;
	}
	
	public String toString() {
		return String.format("%s + %si", this.real, this.imag);
	}
	
	public void sumar(Complejo b) {
		this.real += b.real;
		this.imag += b.imag;
	}
	
}
